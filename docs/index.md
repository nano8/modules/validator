---
layout: home

hero:
    name:    nano/modules/validator
    tagline: validator module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/validator is a simple and minimal validator module for nano
    -   title:   validator
        details: |
                 nano/modules/validator provides a simple way to validate requests
---
