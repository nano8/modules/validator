---
layout: doc
---

<script setup>
const args = {
    useValidator: [
        { 
            type: 'array',
            name: 'data',
        },
        { 
            type: 'array',
            name: 'rules',
        },
    ],
};
</script>

## usage

use the `useValidator` function to validate data in your middleware or straight in your routes

```php
useRouter()->post('/users', function (Request $request): Response {
    useValidator($request->post(), [
        'name'     => 'required|string',
        'email'    => 'required|email',
        'password' => 'required|string',
    ]);
    
    // will throw a bad request exception if validation fails
...
```

## <Types fn="useValidator" r="void" :args="args.useValidator" /> {#useValidator}

```php
useValidator($data, $rules);
```

to see all the available rules and methods on Validation, check out 

[somnambulist-tech/validation](https://github.com/somnambulist-tech/validation#available-rules)

