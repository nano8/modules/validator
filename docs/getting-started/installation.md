---
layout: doc
---

## installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-validator
```

## registering the module

```php
use laylatichy\nano\modules\validator\ValidatorModule;

useNano()->withModule(new ValidatorModule());
```

