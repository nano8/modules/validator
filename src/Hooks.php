<?php

use laylatichy\nano\modules\validator\ValidatorModule;

if (!function_exists('useValidator')) {
    function useValidator(array $data, array $rules): void {
        $validation = useNanoModule(ValidatorModule::class)
            ->validator
            ->validate($data, $rules);

        if ($validation->fails()) {
            useBadRequestException($validation->errors()->all()); // @phpstan-ignore-line
        }
    }
}

