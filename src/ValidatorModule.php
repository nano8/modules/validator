<?php

namespace laylatichy\nano\modules\validator;

use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\Nano;

class ValidatorModule implements NanoModule {
    public Validator $validator;

    public function __construct() {
        // nothing to do here
    }

    public function register(Nano $nano): void {
        if (isset($this->validator)) {
            useNanoException('validator module already registered');
        }

        $this->validator = new Validator();
    }
}
